#!/usr/bin/env bash

NODEJS_HOME='/c/PortableApps/node.js/20.2.0'

export NODE_SKIP_PLATFORM_CHECK='1'
export PATH="${NODEJS_HOME}:${PATH}"
