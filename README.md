# Cookie-Editor
[Cookie-Editor](https://github.com/warren-bank/crx-cookie-editor/tree/extended) is a browser extension/add-on
that lets you efficiently create, edit and delete cookies for the current tab.
Perfect for developing, quickly testing or even manually managing your cookies for your privacy.

## Description
Cookie-Editor is designed to have a simple to use interface that let you do most standard cookie operations quickly.
It is ideal for developing and testing web pages.

You can easily create, edit and delete a cookie for the current page that you are visiting.
There is also a handy button to mass delete all the cookies for the current page.

Cookie-Editor is available for Google Chrome, [Firefox, and Firefox for Android](https://addons.mozilla.org/en-US/firefox/addon/cookie-editor-extended/).
It should be possible to install it on any webkit browser, but keep in mind that only the previous browsers are officially supported.
